using System;
using System.Runtime.InteropServices;

namespace Ktr
{
    public class ZienaLicense : IDisposable
    {
        internal IntPtr raw;
        
        public ZienaLicense()
        {
            raw = ZLM_checkout_license();
            if (raw == IntPtr.Zero)
                throw new ApplicationException("Error checking out Ziena license.");
        }
        
        ~ZienaLicense()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                int flag = ZLM_release_license(raw);
                if (flag != 0)
                    throw new ApplicationException("Error releasing Ziena license.");
                raw = IntPtr.Zero;
                disposed = true;
            }
        }
        
        // native methods
        
        [DllImport("knitro")]
        private static extern IntPtr ZLM_checkout_license();
        
        [DllImport("knitro")]
        private static extern int ZLM_release_license(IntPtr p);
        
    }
}

