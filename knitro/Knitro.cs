using System;
using System.Runtime.InteropServices;

namespace Ktr
{
    public enum ObjectiveGoal
    {
        Minimize = 0,
        Maximize = 1
    }
    
    public enum ObjectiveType
    {
        General   = 0,
        Linear    = 1,
        Quadratic = 2
    }
    
    public enum ConstraintType
    {
        General   = 0,
        Linear    = 1,
        Quadratic = 2
    }
    
    public enum VariableType
    {
        Continuous = 0,
        Integer    = 1,
        Binary     = 2
    }
    
    public enum FunctionType
    {
        Uncertain = 0,
        Convex    = 1,
        Nonconvex = 2
    }

    public enum Algorithm
    {
        Auto      = 0,
        BarDirect = 1,
        BarCG     = 2,
        ActCG     = 3
    }

    public enum GradientOption
    {
        Exact   = 1,
        Forward = 2,
        Central = 3
    }

    public enum HessianOption
    {
        Exact      = 1,
        BFGS       = 2,
        SR1        = 3,
        FiniteDiff = 4,
        Product    = 5,
        LBFGS      = 6
    }
    
    public enum OutputLevel
    {
        None        = 0,
        Summary     = 1,
        Iter10      = 2,
        Iter        = 3,
        IterVerbose = 4,
        IterX       = 5,
        All         = 6
    }
    
    public enum ReturnCode
    {
        Optimal              =    0,
        EvalFC               =    1,
        EvalGA               =    2,
        EvalH                =    3,
        NewPoint             =    6,
        EvalHV               =    7,
        
        NearOptimal          = -100,
        FeasibleXTol         = -101,
        FeasibleNoImprove    = -102,
        FeasibleFTol         = -103,
        
        Infeasible           = -200,
        InfeasibleXTol       = -201,
        InfeasibleNoImprove  = -202,
        InfeasibleMultistart = -203,
        InfeasibleCstrBounds = -204,
        InfeasibleVarBounds  = -205,
        
        Unbounded            = -300,
        
        IterationLimit       = -400,
        TimeLimit            = -401,
        FEvalLimit           = -402,
        
        CallbackError        = -500,
        LpSolverError        = -501,
        EvalError            = -502,
        OutOfMemory          = -503,
        UserTermination      = -504,
        OpenFileError        = -505,
        BadNorF              = -506,
        BadConstraint        = -507,
        BadJacobian          = -508,
        BadHessian           = -509,
        BadConstraintIndex   = -510,
        BadJacobianIndex     = -511,
        BadHessianIndex      = -512,
        BadConstraintBounds  = -513,
        BadVariableBounds    = -514,
        IllegalCall          = -515,
        BadKcPtr             = -516,
        NullPointer          = -517,
        BadIinitialValue     = -518,
        NewPointHalt         = -519,
        BadLicense           = -520,
        BadParameterIinput   = -521,
        
        InternalError        = -600
    }
    
    public delegate int KnitroEvalFC(int n, int m, double[] x, double[] lambda, ref double obj, double[] c, object userParams);
    public delegate int KnitroEvalGA(int n, int m, double[] x, double[] lambda, double[] objGrad, double[] jac, object userParams);
    public delegate int KnitroEvalHV(int n, int m, int nnzH, double[] x, double[] lambda, double[] hessian, double[] hessVector, object userParams);
    internal delegate int InternalCallback(int evalRequest,
                                           int n,
                                           int m,
                                           int nnzJ,
                                           int nnzH,
                                           IntPtr x,
                                           IntPtr lambda,
                                           ref double obj,
                                           IntPtr c,
                                           IntPtr objGrad,
                                           IntPtr jac,
                                           IntPtr hessian,
                                           IntPtr hessVector,
                                           IntPtr userParams);
    
    public class Knitro : IDisposable
    {
        internal class KnitroEvalFCWrapper
        {
            private Knitro owner;
            private KnitroEvalFC func;
            public KnitroEvalFCWrapper(Knitro k, KnitroEvalFC f)
            {
                if (k == null)
                    throw new ArgumentNullException("k");
                if (f == null)
                    throw new ArgumentNullException("f");
                owner = k;
                func = f;
            }
            public int Call(int evalRequest,
                            int n,
                            int m,
                            int nnzJ,
                            int nnzH,
                            IntPtr x,
                            IntPtr z,
                            ref double obj,
                            IntPtr c,
                            IntPtr g,
                            IntPtr J,
                            IntPtr hessian,
                            IntPtr hessVector,
                            IntPtr userParams)
            {
                double[] t_x = (double[])owner.h_x.Target;
                double[] t_z = z != IntPtr.Zero ? (double[])owner.h_z.Target : null;
                double[] t_c = c != IntPtr.Zero ? (double[])owner.h_c.Target : null;
                if (t_x.Length != n)
                    throw new ArgumentException(String.Format("Unexpected number of primal variables (expected {0}, found {1}).", t_x.Length, n), "n");
                if (t_z != null && t_z.Length != n+m)
                    throw new ArgumentException(String.Format("Incompatible length of dual array (expected {0}, found {1}).", n+m, t_z.Length));
                if (t_c != null && t_c.Length != m)
                    throw new ArgumentException(String.Format("Unexpected number of constraints (expected {0}, found {1}).", t_c.Length, m), "m");
                if (owner.h_x.AddrOfPinnedObject() != x)
                    Marshal.Copy(x, t_x, 0, n);
                if (z != IntPtr.Zero && z != owner.h_z.AddrOfPinnedObject())
                    Marshal.Copy(z, t_z, 0, n+m);
                GCHandle uptr = GCHandle.FromIntPtr(userParams);
                int flag = func(n, m, t_x, t_z, ref obj, t_c, uptr.Target);
                if (c != IntPtr.Zero && c != owner.h_c.AddrOfPinnedObject())
                    Marshal.Copy(t_c, 0, c, m);
                return flag;
            }
        }
        
        internal class KnitroEvalGAWrapper
        {
            private Knitro owner;
            private KnitroEvalGA func;
            public KnitroEvalGAWrapper(Knitro k, KnitroEvalGA f)
            {
                if (k == null)
                    throw new ArgumentNullException("k");
                if (f == null)
                    throw new ArgumentNullException("f");
                owner = k;
                func = f;
            }
            public int Call(int evalRequest,
                            int n,
                            int m,
                            int nnzJ,
                            int nnzH,
                            IntPtr x,
                            IntPtr z,
                            ref double obj,
                            IntPtr c,
                            IntPtr g,
                            IntPtr J,
                            IntPtr hessian,
                            IntPtr hessVector,
                            IntPtr userParams)
            {
                double[] t_x = (double[])owner.h_x.Target;
                double[] t_z = z != IntPtr.Zero ? (double[])owner.h_z.Target : null;
                double[] t_g = g != IntPtr.Zero ? (double[])owner.h_g.Target : null;
                double[] t_J = J != IntPtr.Zero ? (double[])owner.h_J.Target : null;
                if (t_x.Length != n)
                    throw new ArgumentException(String.Format("Unexpected number of primal variables (expected {0}, found {1}).", t_x.Length, n), "n");
                if (t_z != null && t_z.Length != n+m)
                    throw new ArgumentException(String.Format("Incompatible length of dual array (expected {0}, found {1}).", n+m, t_z.Length));
                if (t_g != null && t_g.Length != n)
                    throw new ArgumentException(String.Format("Incompatible length of gradient array (expected {0}, found {1}).", n, t_g.Length));
                if (t_J.Length != nnzJ)
                    throw new ArgumentException(String.Format("Unexpected number of Jacobian nonzeros (expected {0}, found {1}).", t_J.Length, nnzJ), "nnzJ");
                if (owner.h_x.AddrOfPinnedObject() != x)
                    Marshal.Copy(x, t_x, 0, n);
                if (z != IntPtr.Zero && z != owner.h_z.AddrOfPinnedObject())
                    Marshal.Copy(z, t_z, 0, n+m);
                GCHandle uptr = GCHandle.FromIntPtr(userParams);
                int flag = func(n, m, t_x, t_z, t_g, t_J, uptr.Target);
                if (g != IntPtr.Zero && g != owner.h_g.AddrOfPinnedObject())
                    Marshal.Copy(t_g, 0, g, n);
                if (J != IntPtr.Zero && J != owner.h_J.AddrOfPinnedObject())
                    Marshal.Copy(t_J, 0, J, nnzJ);
                return flag;
            }
        }
        
        internal class KnitroEvalHVWrapper
        {
            private Knitro owner;
            private KnitroEvalHV func;
            public KnitroEvalHVWrapper(Knitro k, KnitroEvalHV f)
            {
                if (k == null)
                    throw new ArgumentNullException("k");
                if (f == null)
                    throw new ArgumentNullException("f");
                owner = k;
                func = f;
            }
            public int Call(int evalRequest,
                            int n,
                            int m,
                            int nnzJ,
                            int nnzH,
                            IntPtr x,
                            IntPtr z,
                            ref double obj,
                            IntPtr c,
                            IntPtr g,
                            IntPtr J,
                            IntPtr H,
                            IntPtr Hv,
                            IntPtr userParams)
            {
                int flag = 0;
                if (evalRequest == 3)
                {
                    double[] t_x = (double[])owner.h_x.Target;
                    double[] t_z = z != IntPtr.Zero ? (double[])owner.h_z.Target : null;
                    double[] t_H = H != IntPtr.Zero ? (double[])owner.h_H.Target : null;
                    if (t_x.Length != n)
                        throw new ArgumentException(String.Format("Unexpected number of primal variables (expected {0}, found {1}).", t_x.Length, n), "n");
                    if (t_z != null && t_z.Length != n+m)
                        throw new ArgumentException(String.Format("Incompatible length of dual array (expected {0}, found {1}).", n+m, t_z.Length));
                    if (t_H.Length != nnzH)
                        throw new ArgumentException(String.Format("Unexpected number of Jacobian nonzeros (expected {0}, found {1}).", t_H.Length, nnzH), "nnzH");
                    if (owner.h_x.AddrOfPinnedObject() != x)
                        Marshal.Copy(x, t_x, 0, n);
                    if (z != IntPtr.Zero && z != owner.h_z.AddrOfPinnedObject())
                        Marshal.Copy(z, t_z, 0, n+m);
                    GCHandle uptr = GCHandle.FromIntPtr(userParams);
                    flag = func(n, m, nnzH, t_x, t_z, t_H, null, uptr.Target);
                    if (H != IntPtr.Zero && H != owner.h_H.AddrOfPinnedObject())
                        Marshal.Copy(t_H, 0, H, nnzH);
//                    if (Hv != IntPtr.Zero)
//                        Marshal.Copy(t_Hv, 0, Hv, n);
                }
                else if (evalRequest == 7)
                {
                    throw new NotImplementedException();
                }
                return flag;
            }
        }
        
        internal IntPtr raw;
        
        private int n;
        private int m;
        private int nnzJ;
        private int nnzH;
        
        private InternalCallback evalFC;
        private InternalCallback evalGA;
        private InternalCallback evalHV;
        
        private GCHandle h_x;
        private GCHandle h_z;
        private GCHandle h_c;
        private GCHandle h_g;
        private GCHandle h_J;
        private GCHandle h_H;
        private GCHandle h_Hv;
        
        public Knitro()
        {
            raw = KTR_new();
            if (raw == IntPtr.Zero)
                throw new ApplicationException("Error instantiating Knitro solver.");
            // mark as uninitialized
            n = -1;
            m = -1;
            nnzJ = -1;
            nnzH = -1;
        }
        
        public Knitro(ZienaLicense zl)
        {
            raw = KTR_new_zlm(IntPtr.Zero, IntPtr.Zero, zl.raw);
            if (raw == IntPtr.Zero)
                throw new ApplicationException("Error instantiating Knitro solver.");
            // mark as uninitialized
            n = -1;
            m = -1;
            nnzJ = -1;
            nnzH = -1;
        }
        
        ~Knitro()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                int flag = KTR_free(ref raw);
                if (flag != 0)
                    throw new ApplicationException("Error freeing Knitro solver.");
                raw = IntPtr.Zero;
                evalFC = null;
                evalGA = null;
                evalHV = null;
                disposed = true;
            }
        }
        
        // Knitro methods
        
        // unconstrained problem
        public void InitProblem(int n, ObjectiveGoal goal, ObjectiveType type, double[] xinit)
        {
            InitProblem(n, goal, type, null, null, 0, null, null, xinit);
        }
        
        // unconstrained problem with second-order information (Hessian)
        public void InitProblem(int n, ObjectiveGoal goal, ObjectiveType type, int nnzH, int[] Hrows, int[] Hcols, double[] xinit)
        {
            InitProblem(n, goal, type, null, null, nnzH, Hrows, Hcols, xinit);
        }
        
        // box-constrained problem
        public void InitProblem(int n, ObjectiveGoal goal, ObjectiveType type, double[] xLoBnds, double[] xUpBnds, double[] xinit)
        {
            InitProblem(n, goal, type, xLoBnds, xUpBnds, 0, null, null, xinit);
        }
        
        // box-constrained problem with second-order information (Hessian)
        public void InitProblem(int n, ObjectiveGoal goal, ObjectiveType type, double[] xLoBnds, double[] xUpBnds, int nnzH, int[] Hrows, int[] Hcols, double[] xinit)
        {
            if (xLoBnds != null && xLoBnds.Length != n)
                throw new ArgumentException(String.Format("Invalid length of lower bound array (expected {0}, found {1}).", n, xLoBnds.Length));
            if (xUpBnds != null && xUpBnds.Length != n)
                throw new ArgumentException(String.Format("Invalid length of upper bound array (expected {0}, found {1}).", n, xUpBnds.Length));
            if (Hrows != null && Hrows.Length != nnzH)
                throw new ArgumentException(String.Format("Invalid length of Hessian row index array (expected {0}, found {1}).", nnzH, Hrows.Length));
            if (Hcols != null && Hcols.Length != nnzH)
                throw new ArgumentException(String.Format("Invalid length of Hessian column index array (expected {0}, found {1}).", nnzH, Hcols.Length));
            if (xinit != null && xinit.Length != n)
                throw new ArgumentException(String.Format("Invalid length of starting value array (expected {0}, found {1}).", n, xinit.Length));
            GCHandle xLoBndsPtr = GCHandle.Alloc(xLoBnds, GCHandleType.Pinned);
            GCHandle xUpBndsPtr = GCHandle.Alloc(xUpBnds, GCHandleType.Pinned);
            GCHandle HrowsPtr = GCHandle.Alloc(Hrows, GCHandleType.Pinned);
            GCHandle HcolsPtr = GCHandle.Alloc(Hcols, GCHandleType.Pinned);
            GCHandle xinitptr = GCHandle.Alloc(xinit, GCHandleType.Pinned);
            try {
                int flag = KTR_init_problem(raw, n, (int)goal, (int) type, xLoBndsPtr.AddrOfPinnedObject(),
                                            xUpBndsPtr.AddrOfPinnedObject(), 0, IntPtr.Zero, IntPtr.Zero,
                                            IntPtr.Zero, 0, IntPtr.Zero, IntPtr.Zero,
                                            nnzH, HrowsPtr.AddrOfPinnedObject(), HcolsPtr.AddrOfPinnedObject(),
                                            xinitptr.AddrOfPinnedObject(), IntPtr.Zero);
                if (flag != 0)
                    throw new ApplicationException(String.Format("Error initializing problem: {0}.", flag));
                this.n = n;
                this.m = 0;
                this.nnzJ = 0;
                this.nnzH = nnzH;
            } finally {
                xLoBndsPtr.Free();
                xUpBndsPtr.Free();
                HrowsPtr.Free();
                HcolsPtr.Free();
                xinitptr.Free();
            }
        }
        
        public void SetFuncCallback(KnitroEvalFC func)
        {
            KnitroEvalFCWrapper fw = new KnitroEvalFCWrapper(this, func);
            evalFC = new InternalCallback(fw.Call);
            int flag = KTR_set_func_callback(raw, Marshal.GetFunctionPointerForDelegate(evalFC));
            if (flag != 0)
                throw new ApplicationException(String.Format("Error setting function callback: {0}.", flag));
        }
        
        public void SetGradCallback(KnitroEvalGA func)
        {
            KnitroEvalGAWrapper fw = new KnitroEvalGAWrapper(this, func);
            evalGA = new InternalCallback(fw.Call);
            int flag = KTR_set_grad_callback(raw, Marshal.GetFunctionPointerForDelegate(evalGA));
            if (flag != 0)
                throw new ApplicationException(String.Format("Error setting gradient callback: {0}.", flag));
        }
        
        public void SetHessCallback(KnitroEvalHV func)
        {
            KnitroEvalHVWrapper fw = new KnitroEvalHVWrapper(this, func);
            evalHV = new InternalCallback(fw.Call);
            int flag = KTR_set_hess_callback(raw, Marshal.GetFunctionPointerForDelegate(evalHV));
            if (flag != 0)
                throw new ApplicationException(String.Format("Error setting Hessian callback: {0}.", flag));
        }
        
        public ReturnCode Solve(double[] x, double[] z, out double obj)
        {
            return Solve(x, z, null, out obj);
        }
        
        public ReturnCode Solve(double[] x, double[] z, object userParam, out double obj)
        {
            if (x.Length != n)
                throw new ArgumentException(String.Format("Invalid length (expected {0}, found {1}).", n, x.Length), "x");
            if (z.Length != n+m)
                throw new ArgumentException(String.Format("Invalid length (expected {0}, found {1}).", n+m, z.Length), "z");
            // alloc storage for solver arguments
            double[] t_c = new double[m];
            double[] t_g = new double[n];
            double[] t_J = new double[nnzJ];
            double[] t_H = new double[nnzH];
            double[] t_Hv = new double[n];
            // pin arrays
            h_x = GCHandle.Alloc(x, GCHandleType.Pinned);
            h_z = GCHandle.Alloc(z, GCHandleType.Pinned);
            h_c = GCHandle.Alloc(t_c, GCHandleType.Pinned);
            h_g = GCHandle.Alloc(t_g, GCHandleType.Pinned);
            h_J = GCHandle.Alloc(t_J, GCHandleType.Pinned);
            h_H = GCHandle.Alloc(t_H, GCHandleType.Pinned);
            h_Hv = GCHandle.Alloc(t_Hv, GCHandleType.Pinned);
            // handle user data
            GCHandle uptr = GCHandle.Alloc(userParam, GCHandleType.Normal);
            // call solver
            int flag;
            try {
                obj = 0.0;
                flag = KTR_solve(raw, h_x.AddrOfPinnedObject(), h_z.AddrOfPinnedObject(),
                                 0, ref obj, h_c.AddrOfPinnedObject(), h_g.AddrOfPinnedObject(),
                                 h_J.AddrOfPinnedObject(), h_H.AddrOfPinnedObject(), 
                                 h_Hv.AddrOfPinnedObject(), GCHandle.ToIntPtr(uptr));
                if (flag > 0)
                    throw new ApplicationException(String.Format("Reverse communication mode is not supported (request: {0}).", (ReturnCode)flag));
            } finally {
                h_x.Free();
                h_z.Free();
                h_c.Free();
                h_g.Free();
                h_J.Free();
                h_H.Free();
                h_Hv.Free();
            }
            return (ReturnCode)flag;
        }

        // Knitro properties

        public int NumVariables
        {
            get { return n; }
        }

        public int NumConstraints
        {
            get { return m; }
        }

        public Algorithm Algorithm
        {
            get
            {
                int val;
                int flag = KTR_get_int_param_by_name(raw, "algorithm", out val);
                if (flag != 0)
                    throw new ApplicationException("Error getting Algorithm.");
                return (Algorithm)val;
            }
            set
            {
                int flag = KTR_set_int_param_by_name(raw, "algorithm", (int)value);
                if (flag != 0)
                    throw new ApplicationException("Error setting Algorithm.");
            }
        }
        
        public GradientOption Gradient
        {
            get
            {
                int val;
                int flag = KTR_get_int_param_by_name(raw, "gradopt", out val);
                if (flag != 0)
                    throw new ApplicationException("Error getting GradientOption.");
                return (GradientOption)val;
            }
            set
            {
                int flag = KTR_set_int_param_by_name(raw, "gradopt", (int)value);
                if (flag != 0)
                    throw new ApplicationException("Error setting GradientOption.");
            }
        }
        
        public HessianOption Hessian
        {
            get
            {
                int val;
                int flag = KTR_get_int_param_by_name(raw, "hessopt", out val);
                if (flag != 0)
                    throw new ApplicationException("Error getting HessianOption.");
                return (HessianOption)val;
            }
            set
            {
                int flag = KTR_set_int_param_by_name(raw, "hessopt", (int)value);
                if (flag != 0)
                    throw new ApplicationException("Error setting HessianOption.");
            }
        }
        
        public OutputLevel OutputLevel
        {
            get
            {
                int val;
                int flag = KTR_get_int_param_by_name(raw, "outlev", out val);
                if (flag != 0)
                    throw new ApplicationException("Error getting OutputLevel.");
                return (OutputLevel)val;
            }
            set
            {
                int flag = KTR_set_int_param_by_name(raw, "outlev", (int)value);
                if (flag != 0)
                    throw new ApplicationException("Error setting OutputLevel.");
            }
        }
        
        // native methods
        
        [DllImport("knitro")]
        private static extern IntPtr KTR_new();
        
        [DllImport("knitro")]
        private static extern IntPtr KTR_new_zlm(IntPtr fnPtr, IntPtr userParams, IntPtr p);
        
        [DllImport("knitro")]
        private static extern int KTR_free(ref IntPtr p);
        
        [DllImport("knitro")]
        private static extern int KTR_init_problem(IntPtr p, int n, int objGoal, int objType, IntPtr xLoBnds, IntPtr xUpBnds,
                                                   int m, IntPtr cType, IntPtr cLoBnds, IntPtr cUpBnds,
                                                   int nnzJ, IntPtr jacIndexVars, IntPtr jacIndexCons,
                                                   int nnzH, IntPtr hessIndexRows, IntPtr hessIndexRows,
                                                   IntPtr xInitial, IntPtr lambdaInitial);
        
        [DllImport("knitro")]
        private static extern int KTR_solve(IntPtr p,
                                            IntPtr x,
                                            IntPtr z,
                                            int evalStatus,
                                            ref double obj,
                                            IntPtr c,
                                            IntPtr objGrad,
                                            IntPtr jac,
                                            IntPtr hess,
                                            IntPtr hessVector,
                                            IntPtr userParams);
  
        [DllImport("knitro")]
        private static extern int KTR_set_func_callback(IntPtr p, IntPtr callbackEvalFC);
  
        [DllImport("knitro")]
        private static extern int KTR_set_grad_callback(IntPtr p, IntPtr callbackEvalGA);
  
        [DllImport("knitro")]
        private static extern int KTR_set_hess_callback(IntPtr p, IntPtr callbackEvalHV);
  
        [DllImport("knitro")]
        private static extern int KTR_get_int_param_by_name(IntPtr p, string name, out int val);
  
        [DllImport("knitro")]
        private static extern int KTR_set_int_param_by_name(IntPtr p, string name, int val);
  
        [DllImport("knitro")]
        private static extern int KTR_get_double_param_by_name(IntPtr p, string name, out int val);
  
        [DllImport("knitro")]
        private static extern int KTR_set_double_param_by_name(IntPtr p, string name, int val);
    }
}

