using System.Reflection;
using System.Runtime.CompilerServices;

// general stuff

[assembly: AssemblyTitle("knitro")]
[assembly: AssemblyDescription("API and bindings for the Knitro solver.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright (c) 2011 Markus Uhr")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// version stuff

[assembly: AssemblyVersion("1.0.0.*")]

// signing stuff

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]

