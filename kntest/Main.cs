using System;

using Ktr;

namespace KtrTest
{
    class Counter
    {
        public int NumFuncEvals;
        public int NumGradEvals;
    }
    
    class MainClass
    {
        private static int f_exp6(int n, int m, double[] x, double[] z, ref double obj, double[] cstr, object userParams)
        {
            double f = 0.0;
            for (int i = 1; i<=13; i++)
            {
                double c = -(double)i/10.0;
                double fi =   x[2]*Math.Exp(c*x[0]) - x[3]*Math.Exp(c*x[1])
                            + x[5]*Math.Exp(c*x[4]) -      Math.Exp(c)
                            +  5.0*Math.Exp(10.0*c) -  3.0*Math.Exp(4.0*c);
                f += fi*fi;
            }
            obj = f;
            ((Counter)userParams).NumFuncEvals += 1;
            return 0;
        }
        
        private static int g_exp6(int n, int m, double[] x, double[] z, double[] g, double[] jac, object userParams)
        {
            Array.Clear(g, 0, n);
            
            for (int i=1; i<=13; i++)
            {
                double c = -(double)i/10.0;
                double fi =   x[2]*Math.Exp(c*x[0]) - x[3]*Math.Exp(c*x[1])
                            + x[5]*Math.Exp(c*x[4]) -      Math.Exp(c)
                            +  5.0*Math.Exp(10.0*c) -  3.0*Math.Exp(4.0*c);
                
                double dfdx0 =  c*x[2]*Math.Exp(c*x[0]);
                double dfdx1 = -c*x[3]*Math.Exp(c*x[1]);
                double dfdx2 =         Math.Exp(c*x[0]);
                double dfdx3 = -       Math.Exp(c*x[1]);
                double dfdx4 =  c*x[5]*Math.Exp(c*x[4]);
                double dfdx5 =         Math.Exp(c*x[4]);
                
                g[0] += 2.0*fi*dfdx0;
                g[1] += 2.0*fi*dfdx1;
                g[2] += 2.0*fi*dfdx2;
                g[3] += 2.0*fi*dfdx3;
                g[4] += 2.0*fi*dfdx4;
                g[5] += 2.0*fi*dfdx5;
            }
            ((Counter)userParams).NumGradEvals += 1;
            return 0;
        }
        
        public static void Main(string[] args)
        {
//            using (ZienaLicense zl = new ZienaLicense())
//            using (Knitro solver = new Knitro(zl))
            using (Knitro solver = new Knitro())
            {
                int n = 6;
                double[] x = new double[] {1.0, 2.0, 1.0, 1.0, 1.0, 1.0};
                double[] z = new double[n];
                double val = 0.0;

                solver.Algorithm = Algorithm.BarCG;
                solver.Gradient = GradientOption.Exact;
                solver.Hessian = HessianOption.BFGS;
                
                solver.InitProblem(n, ObjectiveGoal.Minimize, ObjectiveType.General, x);
                solver.SetFuncCallback(f_exp6);
                solver.SetGradCallback(g_exp6);
                
//                ReturnCode result = solver.Solve(x, z, out val);
                Counter counter = new Counter();
                ReturnCode result = solver.Solve(x, z, counter, out val);
                if (result != ReturnCode.Optimal)
                {
                    Console.WriteLine("f_exp6 return code: {0}.", result);
                }
                else
                {
                    Console.WriteLine("f_exp6 = {0}  ({1} evaluations)", val, counter.NumFuncEvals);
                    Console.WriteLine("  x[0] = {0}", x[0]);
                    Console.WriteLine("  x[1] = {0}", x[1]);
                    Console.WriteLine("  x[2] = {0}", x[2]);
                    Console.WriteLine("  x[3] = {0}", x[3]);
                    Console.WriteLine("  x[4] = {0}", x[4]);
                    Console.WriteLine("  x[5] = {0}", x[5]);
                }
            }
        }
    }
}

